import { useEffect, useState } from "react";
import "./App.css";
import NavBar from "./components/navbar";
import { ChakraProvider, Checkbox, Container, Flex } from "@chakra-ui/react";
import Search from "./components/search";
import SortCountries from "./components/sortCountries";
import * as RestCountriesApi from "./components/countries";
import RegionFilter from "./components/regionFilter";
import SubRegionFilter from "./components/subRegionFilter";
import Loading from "./components/loading";
import CountryCard from "./components/countryCard";

function App() {
  const [loading, setLoading] = useState(true);
  const [countriesData, setCountriesData] = useState([]);
  const [regionInput, setRegionInput] = useState("");
  const [subRegionInput, setSubRegionInput] = useState("");
  const [inputCountry, setInputCountry] = useState("");
  const [landLockedInput, setLandLockedInput] = useState(false);
  const [sortedValues, setSortedValues] = useState("");

  useEffect(() => {
    RestCountriesApi.countriesData().then((data) => {
      setLoading(false);
      setCountriesData(data);
    });
  });

  //to set regionInput
  function regionValueSet(ClickedRegion) {
    setRegionInput(ClickedRegion);
  }
  //function to set subRegion Input
  function subRegionValue(ClickedSubRegion) {
    setSubRegionInput(ClickedSubRegion);
  }
  //searched Country Data
  function searchCountryValue(searchedCountry) {
    setInputCountry(searchedCountry);
  }
  function landLockedValue(data) {
    setLandLockedInput(data);
  }
  function sortValue(data) {
    setSortedValues(data);
  }

  //regions & sub-regions objects of Data
  let regionsData = countriesData.reduce((acc, country) => {
    acc[country.region] = country.region;
    return acc;
  }, {});
  regionsData = Object.values(regionsData);

  let subRegionData = countriesData.reduce((acc, country) => {
    acc[country.subregion] = country.subregion;
    return acc;
  }, {});
  subRegionData = Object.values(subRegionData);

  //find countries searched by user
  let filteredCountries = countriesData.filter((country) => {
    return country.name.common
      .toLowerCase()
      .includes(inputCountry.toLowerCase().trim());
  });

  //filter as per region
  filteredCountries =
    regionInput === ""
      ? filteredCountries
      : filteredCountries.filter((country) => {
          if (country.region === regionInput) {
            return country;
          }
        });

  //filter as per sub-region
  filteredCountries =
    subRegionInput === ""
      ? filteredCountries
      : filteredCountries.filter((country) => {
          if (country.subregion === subRegionInput) {
            return country;
          }
        });

  //filter as per landLocked boolean value
  filteredCountries = filteredCountries.filter((country) => {
    if (country.landlocked === landLockedInput) {
      return country;
    }
  });

  sortBy(sortedValues);

  function sortBy(sortedValues) {
    if (sortedValues === "") return;
    else if (sortedValues === "population") {
      filteredCountries.sort((a, b) => {
        return b.population - a.population;
      });
    } else {
      filteredCountries.sort((a, b) => {
        return b.area - a.area;
      });
    }
  }
//changing theme 
  function changeTheme(e) {
    if ((e.target.textContent === "LightMode")) {
      document.getElementById("root").style.backgroundColor = "black";
      e.target.textContent = "DarkMode";
    } else {
      document.getElementById("root").style.backgroundColor = "white";
      e.target.textContent = "LightMode";
    }
  }
  return (
    <ChakraProvider>
      <NavBar onChangeTheme={changeTheme} />
      <Container maxWidth={1440}>
        <Flex
          flexDirection={{ base: "column", md: "row" }}
          gap={{ base: "1rem", md: "2rem" }}
          justifyContent={"space-between"}
          mt={3}
        >
          <Search
            searchCountryValue={searchCountryValue}
            inputCountry={inputCountry}
          />
          <SortCountries sortValue={sortValue} />
          <Checkbox
            onChange={(e) => {
              landLockedValue(e.target.checked);
            }}
            
            p={"0.5rem"}

            bg={"#FFFFFF"}
            borderRadius={"1rem"}
            boxShadow={
              "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px"
            }
          >
            landLocked
          </Checkbox>
          <RegionFilter
            regions={regionsData}
            regionValueSet={regionValueSet}
            regionInput={regionInput}
          />
          <SubRegionFilter
            subRegionData={subRegionData}
            subRegionValue={subRegionValue}
            subRegionInput={subRegionInput}
          />
        </Flex>
        {loading ? (
          <Loading />
        ) : (
          <CountryCard countriesData={filteredCountries} />
        )}
      </Container>
    </ChakraProvider>
  );
}
export default App;
