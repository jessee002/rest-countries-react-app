import { Box, Select } from "@chakra-ui/react";
import React from "react";

function RegionFilter({ regions, regionValueSet, regionInput }) {
  let optionsList = regions.map((region, idx) => {
    return (
      <option key={idx} value={region}>
        {region}
      </option>
    );
  });

  return (
    <Box>
      <Select
        width={{md: "10rem"}}
        bg={"#FFFFFF"}
        value={regionInput}
        placeholder="Filter By Regions"
        onChange={(e) => {
          regionValueSet(e.target.value);
        }}
        boxShadow={
          "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px"
        }
        borderRadius={"1rem"}
      >
        {optionsList}
      </Select>
    </Box>
  );
}

export default RegionFilter;
