import { SearchIcon } from "@chakra-ui/icons";
import { Box, Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import React, { Component } from "react";

function Search({searchCountryValue, inputCountry}) {
  return (
    <Box>
      <InputGroup>
        <InputLeftElement children={<SearchIcon color="black.300" />} />
        <Input
          width={{md: "10rem"}}
          bg={"#FFFFFF"}
          value={inputCountry}
          onChange={(e) => {
            searchCountryValue(e.target.value);
          }}
          placeholder="Search Country"
          boxShadow={
            "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px"
          }
          borderRadius={"1rem"}
        />
      </InputGroup>
    </Box>
  );
}
export default Search;
