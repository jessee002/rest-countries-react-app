import axios from 'axios';
import React, { Component } from 'react';

export const countriesData = async() => {
    const res = await axios.get("https://restcountries.com/v3.1/all");
    return res.data;
};