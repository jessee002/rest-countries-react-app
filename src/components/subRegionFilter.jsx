import { Box, Select } from "@chakra-ui/react";
import React, { Component } from "react";

function SubRegionFilter({ subRegionData, subRegionValue, subRegionInput }) {
  let optionList = subRegionData.map((subRegion, idx) => {
    return (
      <option key={idx} value={subRegion}>
        {subRegion}
      </option>
    );
  });
  return (
    <Box>
      <Select
        width={{ md: "10rem" }}
        bg={"#FFFFFF"}
        placeholder="Filter By SubRegion"
        value={subRegionInput}
        onChange={(e) => {
          subRegionValue(e.target.value);
        }}
        boxShadow={
          "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px"
        }
        borderRadius={"1rem"}
      >
        {optionList}
      </Select>
    </Box>
  );
}

export default SubRegionFilter;
