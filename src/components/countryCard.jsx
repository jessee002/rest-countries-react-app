import { Card, CardBody, Flex, Heading, Image, Text } from "@chakra-ui/react";
import React, { Component } from "react";
import { Stack, HStack, VStack } from "@chakra-ui/react";

function CountryCard(data) {
  let countryCards = "";
  if (data.countriesData.length === 0) {
    return <Heading>there's No data present</Heading>;
  } else {
    countryCards = data.countriesData.map((country, idx) => {
      return (
        <Card
          key={idx}
          mt={6}
          margin={"2rem 1rem 0 0"}
          maxW={"sm"}
          boxShadow={
            "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px"
          }
        >
          <CardBody>
            <Image
              src={country.flags.png}
              alt={country.flags.alt}
              height={150}
              width={205}
            />
            <Stack align={"flex-start"} spacing={2}>
              <Heading>{country.name.common}</Heading>
              <VStack align={"flex-start"}>
                <HStack>
                  <Heading size="sm">Population:</Heading>
                  <Text>{country.population}</Text>
                </HStack>
                <HStack>
                  <Heading size="sm">Region:</Heading>
                  <Text>{country.region}</Text>
                </HStack>
                <HStack>
                  <Heading size="sm">Capital:</Heading>
                  <Text>{country.capital}</Text>
                </HStack>
              </VStack>
            </Stack>
          </CardBody>
        </Card>
      );
    });
  }

  return (
    <Flex flexDirection={"row"} justifyContent={"space-evenly"} wrap={"wrap"}>
      {countryCards}
    </Flex>
  );
}

export default CountryCard;
