import { Box, Select } from "@chakra-ui/react";
import React, { Component } from "react";

function SortCountries({ sortValue }) {
  return (
    <Box minWidth={"10rem"}>
      <Select
        onChange={(e) => {
          sortValue(e.target.value);
        }}
        bg={"#FFFFFF"}
        placeholder="Sort By"
        boxShadow={
          "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px"
        }
        borderRadius={"1rem"}
        
      >
        <option key={1} value="population" >
          Sort By Population
        </option>
        <option key={2} value="area">
          Sort By Area
        </option>
      </Select>
    </Box>
  );
}

export default SortCountries;
