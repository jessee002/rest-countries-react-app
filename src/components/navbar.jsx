import { Box, Button, Flex, Spacer } from "@chakra-ui/react";
import React, { Component } from "react";
import moonDark from "/src/assets/images/moon-dark.png";

function NavBar({ onChangeTheme }) {
  return (
    <Box>
      <Flex
        align={"center"}
        color={"blue"}
        p={1}
        style={{ background: "rgba(255,255,255, .8)" }}
      >
        <Box p="4">
          <a href="#">
            <p>Where in the world?</p>
          </a>
        </Box>
        <Spacer />
        <Button
          onClick={(e) => {
            onChangeTheme(e);
          }}
        >
          <img src={moonDark} alt="" />
          <span>LightMode</span>
        </Button>
      </Flex>
    </Box>
  );
}
export default NavBar;
