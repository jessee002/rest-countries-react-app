import { Box, Spinner, Text } from "@chakra-ui/react";
import React, { Component } from "react";

function Loading() {
  return (
    <Box>
      <Spinner />
      <Text>Loading....</Text>
    </Box>
  );
}

export default Loading;
